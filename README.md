## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/sonarr/badges/main/pipeline.svg)](https://gitlab.com/bastillebsd-templates/sonarr/commits/main)

## sonarr
Bastille Template for Sonarr

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/sonarr
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/sonarr
```
